# coding=utf-8
# --------------------------------------------------------------------
# Copyright (C) 1991 - 2023 - EDF R&D - www.code-aster.org
# This file is part of code_aster.
#
# code_aster is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# code_aster is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with code_aster.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------

# person_in_charge: pierre.badel at edf.fr

DEBUT(CODE=_F(NIV_PUB_WEB="INTRANET"), ERREUR=_F(ALARME="EXCEPTION"), IMPR_MACRO="NON")

MA1 = LIRE_MAILLAGE(FORMAT="MED", UNITE=20)

DATAMAC1 = LIRE_TABLE(UNITE=38, FORMAT="LIBRE", SEPARATEUR="\t")

RESU_C1 = CALC_MAC3COEUR(
    TYPE_COEUR="TEST",
    TABLE_N=DATAMAC1,
    MAILLAGE_N=MA1,
    DEFORMATION=_F(UNITE_THYC=32, NIVE_FLUENCE=3),
)

DATAMAC2 = LIRE_TABLE(UNITE=38, FORMAT="LIBRE", SEPARATEUR="\t")

INI_C2 = PERM_MAC3COEUR(
    TYPE_COEUR_N="TEST",
    TYPE_COEUR_NP1="TEST",
    RESU_N=RESU_C1,
    TABLE_N=DATAMAC1,
    TABLE_NP1=DATAMAC2,
    MAILLAGE_NP1=MA1,
)

RESU_C2 = CALC_MAC3COEUR(
    TYPE_COEUR="TEST",
    TABLE_N=DATAMAC2,
    DEFORMATION=_F(RESU_INIT=INI_C2, UNITE_THYC=32, NIVE_FLUENCE=3),
)

post_c1 = RESU_C1.LIST_PARA()["INST"][-1]
post_i2 = INI_C2.LIST_PARA()["INST"][-1]
post_c2 = RESU_C2.LIST_PARA()["INST"][-1]

TABC1_BB = CREA_TABLE(
    RESU=_F(
        RESULTAT=RESU_C1,
        NOM_CHAM="DEPL",
        INST=post_c1,
        NOM_CMP=("DX", "DY", "DZ"),
        GROUP_MA=("CR_B_B", "TG_B_B"),
    )
)

TABV1_BA = CREA_TABLE(
    RESU=_F(
        RESULTAT=RESU_C1,
        NOM_CHAM="VARI_ELGA",
        INST=post_c1,
        NOM_CMP=("V1", "V2", "V3"),
        GROUP_MA=("CR_B_A", "TG_B_A"),
    )
)

TABV1_BB = CREA_TABLE(
    RESU=_F(
        RESULTAT=RESU_C1,
        NOM_CHAM="VARI_ELGA",
        INST=post_c1,
        NOM_CMP=("V1", "V2", "V3"),
        GROUP_MA=("CR_B_B", "TG_B_B"),
    )
)

TABI2_AB = CREA_TABLE(
    RESU=_F(
        RESULTAT=INI_C2,
        NOM_CHAM="DEPL",
        INST=post_i2,
        NOM_CMP=("DX", "DY", "DZ"),
        GROUP_MA=("CR_A_B", "TG_A_B"),
    )
)

TABI2_BA = CREA_TABLE(
    RESU=_F(
        RESULTAT=INI_C2,
        NOM_CHAM="DEPL",
        INST=post_i2,
        NOM_CMP=("DX", "DY", "DZ"),
        GROUP_MA=("CR_B_A", "TG_B_A"),
    )
)

TABC2_BA = CREA_TABLE(
    RESU=_F(
        RESULTAT=RESU_C2,
        NOM_CHAM="DEPL",
        INST=post_c2,
        NOM_CMP=("DX", "DY", "DZ"),
        GROUP_MA=("CR_B_A", "TG_B_A"),
    )
)

TABC2_BB = CREA_TABLE(
    RESU=_F(
        RESULTAT=RESU_C2,
        NOM_CHAM="DEPL",
        INST=post_c2,
        NOM_CMP=("DX", "DY", "DZ"),
        GROUP_MA=("CR_B_B", "TG_B_B"),
    )
)

TABV2_BA = CREA_TABLE(
    RESU=_F(
        RESULTAT=RESU_C2,
        NOM_CHAM="VARI_ELGA",
        INST=post_c2,
        NOM_CMP=("V1", "V2", "V3"),
        GROUP_MA=("CR_B_A", "TG_B_A"),
    )
)

TABV2_BC = CREA_TABLE(
    RESU=_F(
        RESULTAT=RESU_C2,
        NOM_CHAM="VARI_ELGA",
        INST=post_c2,
        NOM_CMP=("V1", "V2", "V3"),
        GROUP_MA=("CR_B_C", "TG_B_C"),
    )
)

TEST_TABLE(
    REFERENCE="AUTRE_ASTER",
    VALE_REFE=0.008268387932507172,
    VALE_CALC=0.008268387932507172,
    NOM_PARA="DX",
    TYPE_TEST="SOMM",
    TABLE=TABC1_BB,
)

TEST_TABLE(
    REFERENCE="AUTRE_ASTER",
    VALE_REFE=0.02373659146851788,
    VALE_CALC=0.02373659146851788,
    NOM_PARA="DY",
    TYPE_TEST="SOMM",
    TABLE=TABC1_BB,
)

TEST_TABLE(
    REFERENCE="AUTRE_ASTER",
    VALE_REFE=-0.023729021711789047,
    VALE_CALC=-0.023729021711789047,
    NOM_PARA="DZ",
    TYPE_TEST="SOMM",
    TABLE=TABC1_BB,
)

TEST_TABLE(
    REFERENCE="AUTRE_ASTER",
    VALE_REFE=0.00908115482066939,
    VALE_CALC=0.00908115482066939,
    NOM_PARA="DX",
    TYPE_TEST="SOMM",
    TABLE=TABI2_BA,
)

TEST_TABLE(
    REFERENCE="AUTRE_ASTER",
    VALE_REFE=0.0247278987565884,
    VALE_CALC=0.0247278987565884,
    NOM_PARA="DY",
    TYPE_TEST="SOMM",
    TABLE=TABI2_BA,
)

TEST_TABLE(
    CRITERE="ABSOLU",
    VALE_CALC=0.0,
    ORDRE_GRANDEUR=0.01,
    NOM_PARA="DZ",
    TYPE_TEST="SOMM",
    TABLE=TABI2_BA,
)

TEST_TABLE(
    REFERENCE="AUTRE_ASTER",
    VALE_REFE=0.024488536778277458,
    VALE_CALC=0.024488536778277458,
    NOM_PARA="DY",
    TYPE_TEST="SOMM",
    TABLE=TABI2_AB,
)

TEST_TABLE(
    REFERENCE="AUTRE_ASTER",
    VALE_REFE=0.02147878778119371,
    VALE_CALC=0.02147878778119371,
    NOM_PARA="DX",
    TYPE_TEST="SOMM",
    TABLE=TABC2_BA,
)

TEST_TABLE(
    REFERENCE="AUTRE_ASTER",
    VALE_REFE=0.03789860602813412,
    VALE_CALC=0.03789860602813412,
    NOM_PARA="DY",
    TYPE_TEST="SOMM",
    TABLE=TABC2_BA,
)

TEST_TABLE(
    CRITERE="ABSOLU",
    VALE_CALC=0.0,
    ORDRE_GRANDEUR=0.01,
    NOM_PARA="DZ",
    TYPE_TEST="SOMM",
    TABLE=TABC2_BA,
)

TEST_TABLE(
    REFERENCE="AUTRE_ASTER",
    VALE_REFE=0.019713998347928068,
    VALE_CALC=0.019713998347928068,
    NOM_PARA="DX",
    TYPE_TEST="SOMM",
    TABLE=TABC2_BB,
)

TEST_TABLE(
    REFERENCE="AUTRE_ASTER",
    VALE_REFE=0.03261094875025143,
    VALE_CALC=0.03261094875025143,
    NOM_PARA="DY",
    TYPE_TEST="SOMM",
    TABLE=TABC2_BB,
)

TEST_TABLE(
    REFERENCE="AUTRE_ASTER",
    VALE_REFE=-0.03259283458390182,
    VALE_CALC=-0.03259283458390182,
    NOM_PARA="DZ",
    TYPE_TEST="SOMM",
    TABLE=TABC2_BB,
)

TEST_TABLE(
    REFERENCE="AUTRE_ASTER",
    VALE_REFE=0.00016257086394376804,
    VALE_CALC=0.00016257086394376804,
    NOM_PARA="V1",
    TYPE_TEST="MAX",
    TABLE=TABV1_BB,
)

TEST_TABLE(
    REFERENCE="AUTRE_ASTER",
    VALE_REFE=3.0,
    VALE_CALC=3.0,
    NOM_PARA="V2",
    TYPE_TEST="MAX",
    TABLE=TABV1_BB,
)

TEST_TABLE(
    REFERENCE="AUTRE_ASTER",
    VALE_REFE=0.0001212890936157815,
    VALE_CALC=0.0001212890936157815,
    NOM_PARA="V3",
    TYPE_TEST="MAX",
    TABLE=TABV1_BB,
)

TEST_TABLE(
    REFERENCE="AUTRE_ASTER",
    VALE_REFE=0.00027843022249306415,
    VALE_CALC=0.00027843022249306415,
    NOM_PARA="V1",
    TYPE_TEST="MAX",
    TABLE=TABV2_BA,
)

TEST_TABLE(
    REFERENCE="AUTRE_ASTER",
    VALE_REFE=6.0,
    VALE_CALC=6.0,
    NOM_PARA="V2",
    TYPE_TEST="MAX",
    TABLE=TABV2_BA,
)

TEST_TABLE(
    REFERENCE="AUTRE_ASTER",
    VALE_REFE=0.00028058732284391426,
    VALE_CALC=0.00028058732284391426,
    NOM_PARA="V3",
    TYPE_TEST="MAX",
    TABLE=TABV2_BA,
)

TEST_TABLE(
    REFERENCE="AUTRE_ASTER",
    VALE_REFE=6.0,
    VALE_CALC=6.0,
    NOM_PARA="V2",
    TYPE_TEST="MAX",
    TABLE=TABV2_BC,
)

FIN()
